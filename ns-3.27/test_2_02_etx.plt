set terminal png
set output "test_2_02_etx.png"

set title "ETX for 3 nodes and link 2-0"

set ylabel "ETX value"
set xlabel "Time (second)"

set border linewidth 2 
set style line 1 linecolor rgb 'red' linetype 1 linewidth 1
set style line 2 linecolor rgb 'blue' linetype 1 linewidth 1  
set grid ytics  
set grid xtics 
plot "drugi_test_1_etx_02.dat" using 1:2 title "ETX" with linespoints  ls 2

