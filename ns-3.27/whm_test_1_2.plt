set terminal png
set output "whm_test_1_2.png"

set title "ETT for 2 nodes"

set ylabel "ETT value"
set xlabel "Time (second)"

set border linewidth 2 
set style line 1 linecolor rgb 'red' linetype 1 linewidth 1  
set grid ytics  
set grid xtics 
plot "prvi_test_ett.dat" using 1:2 title "ETT" with linespoints  ls 1

