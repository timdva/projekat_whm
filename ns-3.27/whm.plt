set terminal png
set output "whm_test_1.png"

set title "ETX and ETT for 2 nodes"

set ylabel "ETX and ETT value"
set xlabel "Time (second)"

set border linewidth 2 
set style line 1 linecolor rgb 'red' linetype 1 linewidth 1
set style line 2 linecolor rgb 'blue' linetype 1 linewidth 1  
set grid ytics  
set grid xtics 
plot "prvi_test_ett.dat" using 1:2 title "ETT" with linespoints  ls 1, \
     "prvi_test_etx.dat" using 1:2 title "ETX" with linespoints  ls 2

