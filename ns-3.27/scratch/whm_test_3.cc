/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <string>
#include "ns3/core-module.h"
#include "ns3/address.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/point-to-point-helper.h" 
#include "ns3/applications-module.h" 
#include "ns3/ipv4-nix-vector-helper.h"
#include "ns3/mobility-module.h"
#include "ns3/random-variable-stream.h"
#include "ns3/wifi-module.h"
#include "ns3/netanim-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/onoff-application.h"
#include "ns3/whmManager.h"

#include <vector>
#include <sstream>
#include <iostream>
#include <iterator>
#include <fstream> 
#include <string>
#include <cassert>
#include "ns3/error-model.h"
 
#include "ns3/aodv-module.h" 
#include "ns3/olsr-module.h"
#include "ns3/dsdv-module.h" 

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("WHM_Example");

//Create variables to test the results 

uint32_t m_packets_sent = 0; 
uint32_t m_ack_sent = 0; 
uint32_t m_packets_received = 0; 
uint32_t m_ack_received = 0; 
std::vector<Address> m_receive_ack;
std::vector<Address> m_receive;

//uint32_t p_rec=0;
uint32_t p_rec_1=0;
uint32_t p_rec_2=0;
Address a_1,a_2;

uint32_t 
counting (std::vector<Address> v, const Address a){
uint32_t c=0;
uint32_t i=0;
for (i=0; i<v.size(); i++){   
       if(a==v.at(i))
            {c++;
}
}
   return c;
}

double
ETX_metric (uint32_t p_sent, uint32_t p_received, uint32_t a_receive)
{

double p_f=0;
double p_r=0;

p_f=(double)p_received/(double)p_sent;
p_r=(double)a_receive/(double)p_received;

return 1/(p_f*p_r);
}

double
ETT_metric (double ETX)
{

      double S=1024*8;
      double B=12000000;
return (ETX*double(S/B));
}

void
SentPacket(std::string context, Ptr<const Packet> p){

    m_packets_sent++;
        
      
        float a;
        a=((float)m_packets_sent-1)/10;
  

       if( a==1 || a==2 || a==3 || a==4 || a==5 || a==6 || a==7 || a==8 || a==9 || a==10)
        { 

            std::cout<<(double)Simulator::Now().GetSeconds()<<" "<< ETX_metric(10,p_rec_1,p_rec_1)<<"\n";
            std::cout<<(double)Simulator::Now().GetSeconds()<<" "<< ETT_metric(ETX_metric(10,p_rec_1,p_rec_1))<<"\n";

          //   std::cout<< "ETX metric for link node_0<------->node_1 is: "<< ETX_metric(10,p_rec_1,p_rec_1)<<"\n";
            // std::cout<< "ETT metric for link node_0<------->node_1 is: "<< ETT_metric(ETX_metric(10,p_rec_1,p_rec_1))<<"\n";

          std::cout<<(double)Simulator::Now().GetSeconds()<<" "<< ETX_metric(10,p_rec_2,p_rec_2)<<"\n";
            std::cout<<(double)Simulator::Now().GetSeconds()<<" "<< ETT_metric(ETX_metric(10,p_rec_2,p_rec_2))<<"\n"; 

       //      std::cout<< "ETX metric for link node_0<------->node_2 is: "<< ETX_metric(10,p_rec_2,p_rec_2)<<"\n";
         //    std::cout<< "ETT metric for link node_0<------->node_2 is: "<< ETT_metric(ETX_metric(10,p_rec_2,p_rec_2))<<"\n";

                m_receive.clear();
                m_receive_ack.clear();
        }

}
void
ReceivedPacket(std::string context, Ptr<const Packet> p, const Address& addr){

    m_packets_received++;
m_receive.push_back(addr);

if(  m_packets_received == 5 )
{
       
        a_1=m_receive.at(2);
        int i;
        for (i=1;i<1000;i++)
        {
                a_2=m_receive.at(i);
                if(a_1!=a_2)
                break;
        }

}

p_rec_1=counting(m_receive,addr); //std::cout<<'\n'<<p_rec_1<<'\t';
p_rec_2=counting(m_receive,addr); //std::cout<<'\n'<<p_rec_2<<'\n';



}

void
ReceivedAck(std::string context, Ptr<const Packet> p, const Address& addr){
 
   m_ack_received++;

m_receive_ack.push_back(addr);


}
void
Ratio(){

    std::cout<< "\nSent (Packets):\t" <<  m_packets_sent
<< "\tReceived (ACK):\t" <<  m_ack_received
    << "\tReceived (Packets):\t" << m_packets_received <<"\n";
}
int
main (int argc, char *argv[])
{
  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_ALL);
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_ALL); 
  LogComponentEnable ("WHM_Example", LOG_LEVEL_ALL);

  Packet::EnablePrinting(); 
  PacketMetadata::Enable ();

  bool      enablePcap = true;
  double    simulationTime = 300;  
  double    numberOfNodes = 3;  
  bool      enableApplication = true; 



  //
  // Explicitly create the nodes required by the topology (shown above).
  //
  NS_LOG_INFO ("Create nodes.");
  NodeContainer nodes;
  nodes.Create (numberOfNodes); 
 
  // Set up WiFi
  WifiHelper wifi;

  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");        
  wifiChannel.AddPropagationLoss ("ns3::TwoRayGroundPropagationLossModel",
                              "SystemLoss", DoubleValue(1),
                              "HeightAboveZ", DoubleValue(1.5));

// For range near 250m
  wifiPhy.Set ("TxPowerStart", DoubleValue(20));
  wifiPhy.Set ("TxPowerEnd", DoubleValue(20));
  wifiPhy.Set ("TxPowerLevels", UintegerValue(1));
  wifiPhy.Set ("TxGain", DoubleValue(0));
  wifiPhy.Set ("RxGain", DoubleValue(0));
  wifiPhy.Set ("EnergyDetectionThreshold", DoubleValue(-61.8));
  wifiPhy.Set ("CcaMode1Threshold", DoubleValue(-64.8));

  // Set 802.11a standard 
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate12Mbps"), "RtsCtsThreshold", UintegerValue (1492));
  wifiPhy.SetChannel (wifiChannel.Create ());
 
  // Add a non-QoS upper mac
  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
  wifiMac.SetType ("ns3::AdhocWifiMac");
 
  NetDeviceContainer devices;
  devices = wifi.Install (wifiPhy, wifiMac, nodes);


  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
                                 "X", StringValue ("100.0"),
                                 "Y", StringValue ("100.0"),
                                 "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=90]"));
  //mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                             "Mode", StringValue ("Time"),
                             "Time", StringValue ("5s"),
                             "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=3.0]"),
                             "Bounds", StringValue ("0|1000|0|1000"));
  mobility.Install (nodes);
 
 
  std::cout << "OLSR routing protocol" << "\n"; 
  
  InternetStackHelper internet;
  OlsrHelper routingProtocol;
  internet.SetRoutingHelper (routingProtocol);
  internet.Install (nodes);

 Ipv4AddressHelper ipv4;
  NS_LOG_INFO ("Assign IP Addresses.");
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer ifcont = ipv4.Assign (devices);

 Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  Ipv4GlobalRoutingHelper::RecomputeRoutingTables();
 whmSend Send;
 if(enableApplication){

      NS_LOG_INFO ("Create Applications.");

      uint16_t port = 9;   // Discard port (RFC 863)
      uint16_t serverPosition = 0; 
      uint16_t clientPosition1 = 1;
      uint16_t clientPosition2 = 2;
 //uint16_t metrica ;

      // UDP connfection from N0 to N5
      std::cout << "  Sender_1 IP address (node_1) :   " << ifcont.GetAddress (clientPosition1) << "\n";
      std::cout << "  Sender_2 IP address (node_2) :   " << ifcont.GetAddress (clientPosition2) << "\n";
      std::cout << "  Receiver IP address (node_0) :   " << ifcont.GetAddress (serverPosition) << "\n";

      whmSendHelper client1 ("ns3::UdpSocketFactory", InetSocketAddress (ifcont.GetAddress (serverPosition), port));
      whmSendHelper client2 ("ns3::UdpSocketFactory", InetSocketAddress (ifcont.GetAddress (serverPosition), port));
   

     // ApplicationContainer apps = client.Install (nodes);
   ApplicationContainer apps = client1.Install (nodes.Get (clientPosition1));
   apps = client2.Install (nodes.Get (clientPosition2));
      apps.Start (Seconds (5.0));
      apps.Stop (Seconds (simulationTime));

      // Create a packet whmServer to receive these packets
      whmReceiveHelper server ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), port));
      apps = server.Install (nodes.Get (serverPosition));
      //apps = server.Install (nodes);
      
      apps.Start (Seconds (0.0));
      apps.Stop (Seconds (simulationTime)); 
       

}        
        
    Config::Connect("/NodeList/*/ApplicationList/*/$ns3::whmSend/Tx", MakeCallback(&SentPacket)); 
    Config::Connect("/NodeList/*/ApplicationList/*/$ns3::whmReceive/Rx", MakeCallback(&ReceivedPacket));
    Config::Connect("/NodeList/*/ApplicationList/*/$ns3::whmSend/Tx_Ack", MakeCallback(&ReceivedAck)); 
    //Config::Connect("/NodeList/*/ApplicationList/*/$ns3::whmReceive/Rx_Ack", MakeCallback(&SentAck));
 
  if(enablePcap){
    wifiPhy.EnablePcapAll ("whm_log"); 
  }

  //
  // Now, do the actual simulation.
  //
  NS_LOG_INFO ("Run Simulation.");  

   AnimationInterface anim("whm_test_2.xml");
 // Run the simulator
  Simulator::Stop (Seconds (simulationTime));
  Simulator::Run ();
// if(enableApplication) {
   //   Ratio();



  //}



}
