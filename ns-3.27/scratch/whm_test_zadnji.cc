/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <string>
#include "ns3/core-module.h"
#include "ns3/address.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/point-to-point-helper.h" 
#include "ns3/applications-module.h" 
#include "ns3/ipv4-nix-vector-helper.h"
#include "ns3/mobility-module.h"
#include "ns3/random-variable-stream.h"
#include "ns3/wifi-module.h"
#include "ns3/netanim-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/onoff-application.h"
#include "ns3/whmManager.h"

#include <vector>
#include <sstream>
#include <iostream>
#include <iterator>
#include <fstream> 
#include <string>
#include <cassert>
#include "ns3/error-model.h"
 
#include "ns3/aodv-module.h" 
#include "ns3/olsr-module.h"
#include "ns3/dsdv-module.h" 

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("WHM_Example");

//Create variables to test the results 

uint32_t m_packets_sent = 0; 
uint32_t m_ack_sent = 0; 
uint32_t m_packets_received = 0; 
uint32_t m_ack_received = 0; 
std::vector<Address> m_receive_ack;
std::vector<Address> m_receive;

//uint32_t p_rec=0;
uint32_t p_rec_1=0;
uint32_t p_rec_2=0;
Address a_1,a_2;

uint32_t 
counting (std::vector<Address> v, const Address a){
uint32_t c=0;
uint32_t i=0;
// std::cout<< a <<"\n";
//std::cout<< v.at(1) <<"\n";
for (i=0; i<v.size(); i++){   
       if(a==v.at(i))
            {c++;//std::cout<< v.at(i) <<"\n";
}
}
//std::cout<< c <<"\n";
   return c;
}

double
ETX_metric (uint32_t p_sent, uint32_t p_received, uint32_t a_receive)
{

double p_f=0;
double p_r=0;

p_f=(double)p_received/(double)p_sent;
p_r=(double)a_receive/(double)p_received;

return 1/(p_f*p_r);
}

double
ETT_metric (double ETX)
{

      double S=1024*8;
      double B=12000000;
return (ETX*double(S/B));
}

void
SentPacket(std::string context, Ptr<const Packet> p){

    m_packets_sent++;
        
        //std::cout<<'\n'<<((float)m_packets_sent-1)/10<<'\n';
        //std::cout<<p_rec<<'\t'<<a_rec<<'\n';
        float a;
        a=((float)m_packets_sent-1)/10;
       // std::cout<<'\n'<<a<<'\n';
        //std::cout<<'\n'<<a_1<<'\n';

       if( a==1 || a==2 || a==3 || a==4 || a==5 || a==6 || a==7 || a==8 || a==9 || a==10)
        { 
             std::cout<< "ETX metric for link node_0<------->node_1 is: "<< ETX_metric(10,p_rec_1,p_rec_1)<<"\n";
             std::cout<< "ETT metric for link node_0<------->node_1 is: "<< ETT_metric(ETX_metric(10,p_rec_1,p_rec_1))<<"\n";

             std::cout<< "ETX metric for link node_0<------->node_2 is: "<< ETX_metric(10,p_rec_2,p_rec_2)<<"\n";
             std::cout<< "ETT metric for link node_0<------->node_2 is: "<< ETT_metric(ETX_metric(10,p_rec_2,p_rec_2))<<"\n";

                m_receive.clear();
                m_receive_ack.clear();
        }
//std::cout << "\tSent:\t" <<  m_packets_sent;

}
void
ReceivedPacket(std::string context, Ptr<const Packet> p, const Address& addr){

    m_packets_received++;
//std::cout<<"RECEIVE PACKET: "<< addr <<"\n";
m_receive.push_back(addr);
//std::cout << "\tReceived:\t" << m_packets_received; 

if(  m_packets_received == 5 )
{
       // std::cout<<'\n'<<m_receive(2)<<'\n'
        a_1=m_receive.at(2);
        int i;
        for (i=1;i<1000;i++)
        {
                a_2=m_receive.at(i);
                if(a_1!=a_2)
                break;
        }
//std::cout<<'\n'<<m_receive.at(1)<<'\n';
//std::cout<<'\n'<<m_receive.at(2)<<'\n';
}

p_rec_1=counting(m_receive,addr); //std::cout<<'\n'<<p_rec_1<<'\t';
p_rec_2=counting(m_receive,addr); //std::cout<<'\n'<<p_rec_2<<'\n';


//a_rec=counting(m_receive_ack,addr);//adresa cvora 0
}

void
ReceivedAck(std::string context, Ptr<const Packet> p, const Address& addr){
 
   m_ack_received++;

m_receive_ack.push_back(addr);

//std::cout << "\tSent ack:\t" << m_ack_sent;
//p_rec=counting(m_receive,addr); //adresa cvora 1 
}
void
Ratio(){

    std::cout<< "\nSent (Packets):\t" <<  m_packets_sent
<< "\tReceived (ACK):\t" <<  m_ack_received
    << "\tReceived (Packets):\t" << m_packets_received <<"\n";
}

int
main (int argc, char *argv[])
{

  /*
Config::SetDefault ("ns3::RateErrorModel::ErrorRate", DoubleValue (0.001));
  Config::SetDefault ("ns3::RateErrorModel::ErrorUnit", StringValue ("ERROR_UNIT_PACKET"));

  Config::SetDefault ("ns3::BurstErrorModel::ErrorRate", DoubleValue (0.01));
  Config::SetDefault ("ns3::BurstErrorModel::BurstSize", StringValue ("ns3::UniformRandomVariable[Min=1|Max=3]"));

  Config::SetDefault ("ns3::OnOffApplication::PacketSize", UintegerValue (210));
  Config::SetDefault ("ns3::OnOffApplication::DataRate", DataRateValue (DataRate ("48kb/s")));

  std::string errorModelType = "ns3::RateErrorModel";

  // Allow the user to override any of the defaults and the above
  // Bind()s at run-time, via command-line arguments 
*/

    Config::SetDefault ("ns3::Ipv4GlobalRouting::RespondToInterfaceEvents", BooleanValue (true));

  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_ALL);
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_ALL); 
  LogComponentEnable ("WHM_Example", LOG_LEVEL_ALL);

  Packet::EnablePrinting(); 
  PacketMetadata::Enable ();

  double    simulationTime = 300;  
  bool      enableApplication = true;  
  double    error_p = 0.0;


  
  CommandLine cmd; 
  cmd.AddValue ("simulationTime", "simulationTime", simulationTime);
  cmd.AddValue ("error_p", "Packet error rate", error_p); 
  cmd.Parse (argc, argv);

  NS_LOG_INFO ("Create nodes.");
  NodeContainer c;
  c.Create (4);
  NodeContainer n0n2 = NodeContainer (c.Get (0), c.Get (2));
  NodeContainer n0n3 = NodeContainer (c.Get (0), c.Get (3));
  NodeContainer n1n3 = NodeContainer (c.Get (3), c.Get (1));
  NodeContainer n2n3 = NodeContainer (c.Get (2), c.Get (3));
 

// Configure the error model
  // Here we use RateErrorModel with packet error rate
  Ptr<UniformRandomVariable> uv = CreateObject<UniformRandomVariable> ();
  uv->SetStream (50);
  RateErrorModel error_model;
  error_model.SetRandomVariable (uv);
  error_model.SetUnit (RateErrorModel::ERROR_UNIT_PACKET);
  error_model.SetRate (error_p);


  InternetStackHelper internet;
//OlsrHelper routingProtocol;
  //internet.SetRoutingHelper (routingProtocol);
  internet.Install (c);

  // We create the channels first without any IP addressing information
  NS_LOG_INFO ("Create channels.");
  PointToPointHelper p2p;
  p2p.SetDeviceAttribute ("DataRate", StringValue ("320Kbps"));
  p2p.SetChannelAttribute ("Delay", StringValue ("10ms"));
  p2p.SetDeviceAttribute ("ReceiveErrorModel", PointerValue (&error_model));
  
  NetDeviceContainer d0d2 = p2p.Install (n0n2);
  NetDeviceContainer d0d3 = p2p.Install (n0n3);
  NetDeviceContainer d1d3 = p2p.Install (n1n3);
  NetDeviceContainer d2d3 = p2p.Install (n2n3);

  

  // Later, we add IP addresses.
  NS_LOG_INFO ("Assign IP Addresses.");
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i0i2 = ipv4.Assign (d0d2);
  ipv4.Assign (d0d2);

  ipv4.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer i1i3 = ipv4.Assign (d1d3);
  ipv4.Assign (d1d3);


  ipv4.SetBase ("10.251.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i0i3= ipv4.Assign (d0d3);
  ipv4.Assign (d0d3);
  
  ipv4.SetBase ("10.250.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i2i3 = ipv4.Assign (d2d3);
  ipv4.Assign (d2d3);


  //std::cout << "OLSR routing protocol" << "\n"; 

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  Ipv4GlobalRoutingHelper::RecomputeRoutingTables();
 
  if(enableApplication){

      NS_LOG_INFO ("Create Applications.");

      uint16_t port = 9;   // Discard port (RFC 863)
     
      
     // apps.Start (Seconds (0.0));
     // apps.Stop (Seconds (simulationTime)); 

      whmSendHelper Client ("ns3::UdpSocketFactory", InetSocketAddress (i1i3.GetAddress (1), port));

      ApplicationContainer apps = Client.Install (c.Get (0));
      apps.Start (Seconds (15.0));
      apps.Stop (Seconds (simulationTime));

      // Create a packet VoipSink to receive these packets
      whmReceiveHelper Sink ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), port));
      apps = Sink.Install (c.Get (1));

      apps.Start (Seconds (15.0));
      apps.Stop (Seconds (simulationTime)); 
 
  }
    Config::Connect("/NodeList/*/ApplicationList/*/$ns3::whmSend/Tx", MakeCallback(&SentPacket)); 
    Config::Connect("/NodeList/*/ApplicationList/*/$ns3::whmReceive/Rx", MakeCallback(&ReceivedPacket));
    Config::Connect("/NodeList/*/ApplicationList/*/$ns3::whmSend/Tx_Ack", MakeCallback(&ReceivedAck));;
 
  
  // Now, do the actual simulation.
  //
  Ptr<Node> n0 = c.Get (0);
  Ptr<Ipv4> ipv41 = n0->GetObject<Ipv4> ();
  
  uint32_t ipv4ifIndex1 = 2;
  Simulator::Schedule (Seconds (15.5),&Ipv4::SetDown,ipv41, ipv4ifIndex1);
  Simulator::Schedule (Seconds (16.5),&Ipv4::SetUp,ipv41, ipv4ifIndex1);


  NS_LOG_INFO ("Run Simulation.");  
}
