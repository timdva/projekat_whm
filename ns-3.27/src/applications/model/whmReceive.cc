#include "ns3/address.h"
#include "ns3/address-utils.h"
#include "ns3/log.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/node.h"
#include "ns3/socket.h"
#include "ns3/udp-socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "whmReceive.h"
#include "whmSend.h" 
#include "ns3/whm-header.h"
#include "ns3/uinteger.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("whmReceive");

NS_OBJECT_ENSURE_REGISTERED (whmReceive);

TypeId 
whmReceive::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::whmReceive")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<whmReceive> ()
    .AddAttribute ("Local",
                   "The Address on which to Bind the rx socket.",
                   AddressValue (),
                   MakeAddressAccessor (&whmReceive::m_local),
                   MakeAddressChecker ())
    .AddAttribute ("Protocol",
                   "The type id of the protocol to use for the rx socket.",
                   TypeIdValue (UdpSocketFactory::GetTypeId ()),
                   MakeTypeIdAccessor (&whmReceive::m_tid),
                   MakeTypeIdChecker ())
    .AddTraceSource ("Rx",
                     "A packet has been received",
                     MakeTraceSourceAccessor (&whmReceive::m_rxTrace),
                     "ns3::Packet::AddressTracedCallback")
    .AddTraceSource ("Rx_Ack",
                     "A ack has been sent",
                     MakeTraceSourceAccessor (&whmReceive::m_rx_ackTrace),
                     "ns3::Packet::AddressTracedCallback")
    .AddAttribute ("Port", "Port on which we listen for incoming packets.",
                   UintegerValue (9),
                   MakeUintegerAccessor (&whmReceive::m_port),
                   MakeUintegerChecker<uint16_t> ()) 
  ;
  return tid;
}

whmReceive::~whmReceive()
{
  NS_LOG_FUNCTION (this);
  m_socket = 0;
  m_totalRx = 0;
  m_sent_ack=0;
  m_received=0;
  ack_size=1;
  m_flow=0;
}

whmReceive::whmReceive()
{
  NS_LOG_FUNCTION (this);
}

uint64_t whmReceive::GetTotalRx () const
{
  NS_LOG_FUNCTION (this);
  return m_totalRx;
}

Ptr<Socket>
whmReceive::GetListeningSocket (void) const
{
  NS_LOG_FUNCTION (this);
  return m_socket;
}

std::list<Ptr<Socket> >
whmReceive::GetAcceptedSockets (void) const
{
  NS_LOG_FUNCTION (this);
  return m_socketList;
}

void whmReceive::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  m_socket = 0;
  m_socketList.clear ();

  // chain up
  Application::DoDispose ();
}


// Application Methods
void whmReceive::StartApplication ()    // Called at time specified by Start
{
  NS_LOG_FUNCTION (this);
  // Create the socket if not already
  if (!m_socket)
    {
      m_socket = Socket::CreateSocket (GetNode (), m_tid);
      if (m_socket->Bind (m_local) == -1)
        {
          NS_FATAL_ERROR ("Failed to bind socket");
        }
      m_socket->Listen ();
   //   m_socket->ShutdownSend ();
      if (addressUtils::IsMulticast (m_local))
        {
          Ptr<UdpSocket> udpSocket = DynamicCast<UdpSocket> (m_socket);
          if (udpSocket)
            {
              // equivalent to setsockopt (MCAST_JOIN_GROUP)
              udpSocket->MulticastJoinGroup (0, m_local);
            }
          else
            {
              NS_FATAL_ERROR ("Error: joining multicast on a non-UDP socket");
            }
        }
    }

  m_socket->SetRecvCallback (MakeCallback (&whmReceive::HandleRead, this));
  /*m_socket->SetAcceptCallback (MakeNullCallback<bool, Ptr<Socket>, const Address &> (),
  MakeCallback (&whmReceive::HandleAccept, this));m_socket->SetCloseCallbacks (MakeCallback (&whmReceive::HandlePeerClose, this),MakeCallbac    (&whmReceive::HandlePeerError, this));*/
}

void whmReceive::StopApplication ()     // Called at time specified by Stop
{
  NS_LOG_FUNCTION (this);
  while(!m_socketList.empty ()) //these are accepted sockets, close them
    {
      Ptr<Socket> acceptedSocket = m_socketList.front ();
      m_socketList.pop_front ();
      acceptedSocket->Close ();
    }
  if (m_socket) 
    {
      m_socket->Close ();
      m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
    }
}

void whmReceive::HandleRead (Ptr<Socket> socket)
{
  
  whmHeader whmHead;

  NS_LOG_FUNCTION (this << socket);
  Ptr<Packet> packet;
  Ptr<Packet> ack=Create<Packet> (1); //kreiramo ack
  whmHead.SetSeq (m_sent_ack);
  whmHead.SetFlow (m_flow);
  ack->AddHeader (whmHead);
  Address from;
  while ((packet = socket->RecvFrom (from)))
    { 
      m_received++;

	//ispis za provjeru
	//std::cout << "\nReceived (iz whmReceive): " <<  m_received-26144;

      if (packet->GetSize () == 0)
        { //EOF
          break;
        }

      m_rxTrace (packet, InetSocketAddress::ConvertFrom(from).GetIpv4 () );
      
      packet->RemoveHeader(whmHead);
      NS_ASSERT (whmHead.GetSeq() >= 0 );
 
      m_totalRx += packet->GetSize ();

      NS_LOG_INFO ("whm Header detected" << whmHead);

      if (InetSocketAddress::IsMatchingType (from))
        {
	
	 std::cout <<"\nAt time " << Simulator::Now ().GetSeconds () << "s packet Node1 received "<<  packet->GetSize () << " bytes from "
                       << InetSocketAddress::ConvertFrom(from).GetIpv4 () << " port " << InetSocketAddress::ConvertFrom (from).GetPort ()
                       << " total Rx " << m_totalRx << " bytes";
          NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds () << "s packet Node1 received "<<  packet->GetSize () << " bytes from "
                       << InetSocketAddress::ConvertFrom(from).GetIpv4 () << " port " << InetSocketAddress::ConvertFrom (from).GetPort ()
                       << " total Rx " << m_totalRx << " bytes");

          
        }

      packet->RemoveAllPacketTags ();
      packet->RemoveAllByteTags ();

//  NS_LOG_LOGIC ("Echoing packet");
      socket->SendTo (ack, 0, from );
     m_rx_ackTrace (ack, InetSocketAddress::ConvertFrom (from).GetIpv4 () );

      if (InetSocketAddress::IsMatchingType (from))
        {
		m_sent_ack++;

	
std::cout <<"\nAt time " << Simulator::Now ().GetSeconds () << "s Node sent ack " << ack->GetSize () << " bytes to " <<
                       InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
                       InetSocketAddress::ConvertFrom (from).GetPort ();
          NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds () << "s Node1 sent ack " << ack->GetSize () << " bytes to " <<
                       InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
                       InetSocketAddress::ConvertFrom (from).GetPort ());
//m_rx_ackTrace (ack);

       
        }
    }
}


void whmReceive::HandlePeerClose (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
}
 
void whmReceive::HandlePeerError (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
}
 

void whmReceive::HandleAccept (Ptr<Socket> s, const Address& from)
{
  NS_LOG_FUNCTION (this << s << from);
  s->SetRecvCallback (MakeCallback (&whmReceive::HandleRead, this));
  m_socketList.push_back (s);
}

} // Namespace ns3
