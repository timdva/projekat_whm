#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/string.h"
#include "whmSend.h"
#include "whmReceive.h"
#include "ns3/whm-header.h"
#include <cstdlib>
#include <cstdio>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("whmSend");

NS_OBJECT_ENSURE_REGISTERED (whmSend);

TypeId
whmSend::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::whmSend")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<whmSend> ()
    .AddAttribute ("MaxPackets",
                   "The maximum number of packets the application will send",
                   UintegerValue (20),
                   MakeUintegerAccessor (&whmSend::m_count),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("FlowNumber",
                   "The number of flow",
                   UintegerValue (1),
                   MakeUintegerAccessor (&whmSend::m_flow),
                   MakeUintegerChecker<uint32_t> ())  
    .AddAttribute ("Interval",
                   "The time to wait between packets", TimeValue (Seconds (1.0)),
                   MakeTimeAccessor (&whmSend::m_interval),
                   MakeTimeChecker ())
    .AddAttribute ("RemoteAddress",
                   "The destination Address of the outbound packets",
                   AddressValue (),
                   MakeAddressAccessor (&whmSend::m_peerAddress),
                   MakeAddressChecker ())
    .AddAttribute ("RemotePort", "The destination port of the outbound packets",
                   UintegerValue (9),
                   MakeUintegerAccessor (&whmSend::m_peerPort),
                   MakeUintegerChecker<uint16_t> ())
    .AddAttribute ("Packetsize",
                   "Size of packets generated. The minimum packet size is 12 bytes which is the size of the header carrying the sequence number and the time stamp.",
                   UintegerValue (1024),
                   MakeUintegerAccessor (&whmSend::m_size),
                   MakeUintegerChecker<uint32_t> (12,1500)) 
    .AddTraceSource ("Tx",
                     "A packet has been sent",
                     MakeTraceSourceAccessor (&whmSend::m_txTrace),
                     "ns3::Packet::AddressTracedCallback")
    .AddTraceSource ("Tx_Ack",
                     "A ack has been received",
                     MakeTraceSourceAccessor (&whmSend::m_tx_ackTrace),
                     "ns3::Packet::AddressTracedCallback")
  ;
  return tid;
}

whmSend::whmSend ()
{
  NS_LOG_FUNCTION (this);
  m_sent = 0;
  m_socket = 0;
  m_flow = 0;
  m_received_ack=0;
  m_sendEvent = EventId ();

}

whmSend::~whmSend ()
{
  NS_LOG_FUNCTION (this);
}

void
whmSend::SetRemote (Address ip, uint16_t port)
{
  NS_LOG_FUNCTION (this << ip << port);
  m_peerAddress = ip;
  m_peerPort = port;
}

void
whmSend::SetRemote (Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  m_peerAddress = addr;
}


void
whmSend::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  Application::DoDispose ();
}

void
whmSend::StartApplication (void)
{
  NS_LOG_FUNCTION (this);

  if (m_socket == 0)
    {
      TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
      m_socket = Socket::CreateSocket (GetNode (), tid);
      if (Ipv4Address::IsMatchingType(m_peerAddress) == true)
        {
          if (m_socket->Bind () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort));
        }
      else if (InetSocketAddress::IsMatchingType (m_peerAddress) == true)
        {
          if (m_socket->Bind () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
          m_socket->Connect (m_peerAddress);
        }
      else
        {
          NS_ASSERT_MSG (false, "Incompatible address type: " << m_peerAddress);
        }
    }


 // m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
  m_socket->SetRecvCallback (MakeCallback (&whmSend::HandleRead, this));
  m_socket->SetAllowBroadcast (true);
  m_sendEvent = Simulator::Schedule (Seconds (0.0), &whmSend::Send, this);
  
}

void
whmSend::StopApplication (void)
{
  NS_LOG_FUNCTION (this);

  if (m_socket != 0) 
    {
      m_socket->Close ();
      m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
      m_socket = 0;
    }

  Simulator::Cancel (m_sendEvent);

}

void
whmSend::Send (void)
{
  NS_LOG_FUNCTION (this);
  NS_ASSERT (m_sendEvent.IsExpired ());



  whmHeader whmHead;
  whmHead.SetSeq (m_sent);
  whmHead.SetFlow (m_flow);
  
  Ptr<Packet> p = Create<Packet> (m_size-(8+4)); // 8+4 : the size of the whmHead header

  //std::ostringstream msg; msg << "whm primjer!" << '\0';
  //Ptr<Packet> p = Create<Packet> ((uint8_t*) msg.str().c_str(), msg.str().length());

  p->AddHeader (whmHead);

  std::stringstream peerAddressStringStream;

  if (Ipv4Address::IsMatchingType (m_peerAddress))
    {
      peerAddressStringStream << Ipv4Address::ConvertFrom (m_peerAddress);


    }
 
  if ((m_socket->Send (p)) >= 0)
    {
      ++m_sent;	


	std::cout << "\nAt time " << Simulator::Now ().GetSeconds () << "s client sent " << m_size << " bytes to " <<
                   InetSocketAddress::ConvertFrom (m_peerAddress).GetIpv4 () << " port " << m_peerAddress;

       NS_LOG_INFO ("TraceDelay TX " << m_size << " bytes to "
                                    << peerAddressStringStream.str () << " Uid: "
                                    << p->GetUid () << " Time: "
                                    << (Simulator::Now ()).GetSeconds ());
  m_txTrace (p);
    }


   else if (InetSocketAddress::IsMatchingType (m_peerAddress))
    {
     /* NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds () << "s client sent " << m_size << " bytes to " <<
                   InetSocketAddress::ConvertFrom (m_peerAddress).GetIpv4 () << " port " << InetSocketAddress::ConvertFrom (m_peerAddress).GetPort ());*/
    }
else
    {
      NS_LOG_INFO ("Error while sending " << m_size << " bytes to "
                                          << peerAddressStringStream.str ());
    }

  if (m_sent < m_count)
    {
      m_sendEvent = Simulator::Schedule (m_interval, &whmSend::Send, this);
    }

}

void
whmSend::HandleRead (Ptr<Socket> socket)
{

  NS_LOG_FUNCTION (this << socket);
  Ptr<Packet> packet;
  Address from;

  while ((packet = socket->RecvFrom (from)))
    {

       m_tx_ackTrace (packet,InetSocketAddress::ConvertFrom (from).GetIpv4 ());

      if (InetSocketAddress::IsMatchingType (from))
        {
        std::cout <<"\nAt time " << Simulator::Now ().GetSeconds () << "s client received " << packet->GetSize () << " bytes from " <<
                       InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
                       InetSocketAddress::ConvertFrom (from).GetPort ();

          NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds () << "s client received " << packet->GetSize () << " bytes from " <<
                       InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
                       InetSocketAddress::ConvertFrom (from).GetPort ());
        }
     
    }



}

} // Namespace ns3
