#ifndef whm_SINK_H
#define whm_SINK_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/traced-callback.h"
#include "ns3/address.h"

namespace ns3 {

class Address;
class Socket;
class Packet;


class whmReceive : public Application 
{
public:

  static TypeId GetTypeId (void);
  whmReceive ();

  virtual ~whmReceive ();

  uint64_t GetTotalRx () const;

  Ptr<Socket> GetListeningSocket (void) const;

  std::list<Ptr<Socket> > GetAcceptedSockets (void) const;
 
protected:
  virtual void DoDispose (void);
private:
  // inherited from Application base class.
  virtual void StartApplication (void);    // Called at time specified by Start
  virtual void StopApplication (void);     // Called at time specified by Stop

  void HandleRead (Ptr<Socket> socket);
 
  void HandleAccept (Ptr<Socket> socket, const Address& from);
  
  void HandlePeerClose (Ptr<Socket> socket);
 
  void HandlePeerError (Ptr<Socket> socket);

  Ptr<Socket>     m_socket;       //!< Listening socket
  std::list<Ptr<Socket> > m_socketList; //!< the accepted sockets

  uint32_t        m_flow;
  Address         m_local;        //!< Local address to bind to
  uint64_t        m_totalRx;      //!< Total bytes received
  uint16_t        m_sent_ack;      //!< dodano
  uint16_t        m_received;      //!< dodano
  TypeId          m_tid;          //!< Protocol TypeId
  uint16_t        m_port;         //!< Dodan port na kojem cemo ocekivati dolazece pakete
  uint16_t        ack_size;        //dodano

  /// Traced Callback: received packets, source address.
 TracedCallback<Ptr<const Packet>, const Address &> m_rx_ackTrace;//dodano
 TracedCallback<Ptr<const Packet>, const Address &> m_rxTrace;

};

} // namespace ns3

#endif /* whm_SINK_H */

