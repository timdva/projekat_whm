#ifndef whm_CLIENT_HELPER_H
#define whm_CLIENT_HELPER_H

#include <stdint.h>
#include <string>
#include "ns3/object-factory.h"
#include "ns3/address.h"
#include "ns3/attribute.h"
#include "ns3/net-device.h"
#include "ns3/node-container.h"
#include "ns3/application-container.h"
#include "ns3/whmSend.h"

namespace ns3 {

class DataRate;

class whmSendHelper
{
public:

  whmSendHelper (std::string protocol, Address address);

  void SetAttribute (std::string name, const AttributeValue &value);

  void SetConstantRate (DataRate dataRate, uint32_t packewhmze = 512);

  ApplicationContainer Install (NodeContainer c);

  ApplicationContainer Install (Ptr<Node> node);

  ApplicationContainer Install (std::string nodeName);

  uint32_t m_numberOfFlows;
 
private:
 
  Ptr<Application> InstallPriv (Ptr<Node> node);

  void IncreaseFlowNumber();

  ObjectFactory m_factory; //!< Object factory.
};

} // namespace ns3

#endif /* whm_CLIENT_HELPER_H */

