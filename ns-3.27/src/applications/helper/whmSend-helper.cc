#include "whmSend-helper.h"
#include "ns3/inet-socket-address.h"
#include "ns3/packet-socket-address.h"
#include "ns3/string.h"
#include "ns3/data-rate.h"
#include "ns3/uinteger.h"
#include "ns3/names.h"
#include "ns3/random-variable-stream.h"
#include "ns3/whmSend.h"

namespace ns3 {

whmSendHelper::whmSendHelper (std::string protocol, Address address)
{
  m_factory.SetTypeId ("ns3::whmSend"); 
  m_factory.Set ("RemoteAddress", AddressValue (address));
  m_numberOfFlows = 0;
}

void 
whmSendHelper::SetAttribute (std::string name, const AttributeValue &value)
{
  m_factory.Set (name, value);
}

ApplicationContainer
whmSendHelper::Install (Ptr<Node> node)
{
  return ApplicationContainer (InstallPriv (node));
}

ApplicationContainer
whmSendHelper::Install (std::string nodeName)
{
  Ptr<Node> node = Names::Find<Node> (nodeName);
  return ApplicationContainer (InstallPriv (node));
}

ApplicationContainer
whmSendHelper::Install (NodeContainer c)
{
  ApplicationContainer apps;
  for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
      apps.Add (InstallPriv (*i));
    }

  return apps;
}

void 
whmSendHelper::IncreaseFlowNumber(){
  
  m_numberOfFlows++;
  m_factory.Set ("FlowNumber", UintegerValue (m_numberOfFlows));
}

Ptr<Application>
whmSendHelper::InstallPriv (Ptr<Node> node)
{
  IncreaseFlowNumber();

  Ptr<Application> app = m_factory.Create<Application> ();
  node->AddApplication (app);

  return app;
}
 
void 
whmSendHelper::SetConstantRate (DataRate dataRate, uint32_t packetsize)
{
  /*
  m_factory.Set ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1000]"));
  m_factory.Set ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
  m_factory.Set ("DataRate", DataRateValue (dataRate));
  m_factory.Set ("Packewhmze", UintegerValue (packewhmze));
  */
}

} // namespace ns3
