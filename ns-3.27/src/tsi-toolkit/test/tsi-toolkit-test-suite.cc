/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

// Include a header file from your module to test.
#include "ns3/tsi-toolkit.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/point-to-point-helper.h" 
#include "ns3/applications-module.h" 
#include "ns3/ipv4-nix-vector-helper.h"
#include "ns3/mobility-module.h"
#include "ns3/random-variable-stream.h"
#include "ns3/wifi-module.h"
#include "ns3/aodv-module.h" 
#include "ns3/olsr-module.h"
#include "ns3/dsdv-module.h"  
 
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <iterator>
#include <fstream> 

// An essential include is test.h
#include "ns3/test.h"

// Do not put your test classes in namespace ns3.  You may find it useful
// to use the using directive to access the ns3 namespace directly
using namespace ns3;


/*
All ns-3 tests are classified into Test Suites and Test Cases. A test suite is a collection of test cases that completely exercise a given kind of functionality.
As described above, test suites can be classified as,

Build Verification Tests
Unit Tests
System Tests
Examples
Performance Tests

Here we crete one Test Suit and Two Test Cases (TsiToolkitTestCase1 && TsiToolkitTestCase2) for UNIT testing.

*/













// This is an example TestCase 1 - with ns3::ConstantRandomVariable[Constant=3.0]
class TsiToolkitTestCase1 : public TestCase
{
public:
  TsiToolkitTestCase1 ();
  TsiToolkitTestCase1 (uint32_t nNodes, uint32_t simulationTime);
  virtual ~TsiToolkitTestCase1 ();

private:
  uint32_t m_numberOfNodes;
  uint32_t m_simulationTime;
  //Create variables to test the results 
  uint32_t m_bytes_sent; 
  uint32_t m_bytes_received; 

  uint32_t m_packets_sent; 
  uint32_t m_packets_received; 
 
  //Create c++ map to measure delay
  std::map<uint32_t, double> m_delayTable;

  virtual void DoRun (void);
  void SentPacket(std::string context, Ptr<const Packet> p );
  void ReceivedPacket(std::string context, Ptr<const Packet> p, const Address& addr);
  void Ratio();
};

/**
   * \brief Constructor
   * \param context - set the number of nodes
   * \param uint_32t - integer value defining number of nodes in the test
   */ 
TsiToolkitTestCase1::TsiToolkitTestCase1(uint32_t nNodes, uint32_t simulationTime)
: TestCase ("Test Case 1"),
  m_numberOfNodes (nNodes),
  m_simulationTime (simulationTime)
{
  m_packets_sent = 0;
  m_packets_received = 0; 
  m_bytes_sent = 0;
  m_bytes_received = 0; 
}

/**
   * \brief Each time the packet is sent, this function is called using TracedCallback and information about packet is stored in m_delayTable
   * \param context - used to display full path of the information provided (useful for taking information about the nodes or applications)
   * \param packet - Ptr reference to the packet itself
   */
void
TsiToolkitTestCase1::SentPacket(std::string context, Ptr<const Packet> p){
    /* 
    //HELP LINES USED FOR TESTING
    std::cout << "\n ..................SentPacket....." << p->GetUid() << "..." <<  p->GetSize() << ".......  \n";
    p->Print(std::cout);                
    std::cout << "\n ............................................  \n";  
    */

    //Sum bytes of the packet that was sent
    m_bytes_sent += p->GetSize(); 
    m_packets_sent++;

    //Insert in the delay table details about the packet that was sent
    m_delayTable.insert (std::make_pair (p->GetUid(), (double)Simulator::Now().GetSeconds()));
}

void
TsiToolkitTestCase1::ReceivedPacket(std::string context, Ptr<const Packet> p, const Address& addr){
    /*
    //HELP LINES USED FOR TESTING
    std::cout << "\n ..................ReceivedPacket....." << p->GetUid() << "..." <<  p->GetSize() << ".......  \n";
    p->Print(std::cout);                
    std::cout << "\n ............................................  \n";  
    */

    //Find the record in m_delayTable based on packetID
    std::map<uint32_t, double >::iterator i = m_delayTable.find ( p->GetUid() );
    
    //Get the current time in the temp variable
    double temp = (double)Simulator::Now().GetSeconds();  
    //Display the delay for the packet in the form of "packetID delay" where delay is calculated as the current time - time when the packet was sent
    std::cout << p->GetUid() << "\t" << temp - i->second << "\n";

    //Remove the entry from the delayTable to clear the RAM memroy and obey memory leakage
    if(i != m_delayTable.end()){
        m_delayTable.erase(i);
    }

    //Sum bytes and number of packets that were sent
    m_bytes_received += p->GetSize(); 
    m_packets_received++;
}

void
TsiToolkitTestCase1::Ratio(){

    std::cout << "Sent (bytes):\t" <<  m_bytes_sent
    << "\tReceived (bytes):\t" << m_bytes_received 
    << "\nSent (Packets):\t" <<  m_packets_sent
    << "\tReceived (Packets):\t" << m_packets_received 
    
    << "\nRatio (bytes):\t" << (float)m_bytes_received/(float)m_bytes_sent
    << "\tRatio (packets):\t" << (float)m_packets_received/(float)m_packets_sent << "\n";
}

// Add some help text to this case to describe what it is intended to test
TsiToolkitTestCase1::TsiToolkitTestCase1 ()
  : TestCase ("TsiToolkit test case with random mobility"),
  m_numberOfNodes (3),
  m_simulationTime (300)
{
}

// This destructor does nothing but we include it as a reminder that
// the test case should clean up after itself
TsiToolkitTestCase1::~TsiToolkitTestCase1 ()
{
}

//
// This method is the pure virtual method from class TestCase that every
// TestCase must implement
//
void
TsiToolkitTestCase1::DoRun (void)
{
  //
  // Explicitly create the nodes required by the topology (shown above).
  // 
  NodeContainer nodes;
  nodes.Create (m_numberOfNodes); 

  // Set up WiFi
  WifiHelper wifi;

  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");        
  wifiChannel.AddPropagationLoss ("ns3::TwoRayGroundPropagationLossModel",
                              "SystemLoss", DoubleValue(1),
                              "HeightAboveZ", DoubleValue(1.5));

  // For range near 250m
  wifiPhy.Set ("TxPowerStart", DoubleValue(33));
  wifiPhy.Set ("TxPowerEnd", DoubleValue(33));
  wifiPhy.Set ("TxPowerLevels", UintegerValue(1));
  wifiPhy.Set ("TxGain", DoubleValue(0));
  wifiPhy.Set ("RxGain", DoubleValue(0));
  wifiPhy.Set ("EnergyDetectionThreshold", DoubleValue(-61.8));
  wifiPhy.Set ("CcaMode1Threshold", DoubleValue(-64.8));

  // Set 802.11a standard 
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate12Mbps"), "RtsCtsThreshold", UintegerValue (1492));
  wifiPhy.SetChannel (wifiChannel.Create ());

  // Add a non-QoS upper mac
  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
  wifiMac.SetType ("ns3::AdhocWifiMac");

  NetDeviceContainer devices;
  devices = wifi.Install (wifiPhy, wifiMac, nodes);

  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
                                 "X", StringValue ("100.0"),
                                 "Y", StringValue ("100.0"),
                                 "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=90]"));

  //mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                             "Mode", StringValue ("Time"),
                             "Time", StringValue ("5s"),
                             "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=3.0]"),
                             "Bounds", StringValue ("0|1000|0|1000"));
  mobility.Install (nodes);
 
  InternetStackHelper internet;
  OlsrHelper routingProtocol;
  internet.SetRoutingHelper (routingProtocol);
  internet.Install (nodes);

  // Set up Addresses
  Ipv4AddressHelper ipv4; 
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer ifcont = ipv4.Assign (devices);
   
  uint16_t port = 9;   // Discard port (RFC 863)
  uint16_t serverPosition = m_numberOfNodes-1; 
  uint16_t clientPosition = 0;
 
  TsiClientHelper tsiClient ("ns3::UdpSocketFactory", InetSocketAddress (ifcont.GetAddress (serverPosition), port));
  tsiClient.SetAttribute("PacketSize", UintegerValue (150));

  ApplicationContainer apps = tsiClient.Install (nodes.Get (clientPosition));
  apps.Start (Seconds (15.0));
  apps.Stop (Seconds (m_simulationTime));

  // Create a packet tsiSink to receive these packets
  TsiSinkHelper tsiSink ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), port));
  apps = tsiSink.Install (nodes.Get (serverPosition));

  apps.Start (Seconds (15.0));
  apps.Stop (Seconds (m_simulationTime)); 
 
  Config::Connect("/NodeList/*/ApplicationList/*/$ns3::TsiClient/Tx", MakeCallback (&TsiToolkitTestCase1::SentPacket, this)); 
  Config::Connect("/NodeList/*/ApplicationList/*/$ns3::TsiSink/Rx",   MakeCallback(&TsiToolkitTestCase1::ReceivedPacket, this)); 


  Simulator::Stop (Seconds (500));
  Simulator::Run ();

  /*****************************
  * TEST COMMANDS
  *****************************/

  // A wide variety of test macros are available in src/core/test.h and on https://www.nsnam.org/doxygen/group__testing.html
  NS_TEST_ASSERT_MSG_GT (m_packets_sent, 30, "At least 30 packets should be sent");

  // Use this one for floating point comparisons
  NS_TEST_ASSERT_MSG_GT_OR_EQ ((float)m_packets_received/(float)m_packets_sent, 0.70, "At least 70\% of packet delivery ratio");


  Simulator::Destroy ();
}






















// This is an example TestCase 2 - with ns3::ConstantRandomVariable[Constant=10.0]
class TsiToolkitTestCase2 : public TestCase
{
public:
  TsiToolkitTestCase2 ();
  TsiToolkitTestCase2 (uint32_t nNodes, uint32_t simulationTime, uint32_t mobilitySpeed);
  virtual ~TsiToolkitTestCase2 ();

private:
  uint32_t m_numberOfNodes;
  uint32_t m_simulationTime;
  //Create variables to test the results 
  uint32_t m_bytes_sent; 
  uint32_t m_bytes_received; 

  uint32_t m_packets_sent; 
  uint32_t m_packets_received;
 
  uint32_t m_mobility_speed;
 
  //Create c++ map to measure delay
  std::map<uint32_t, double> m_delayTable;

  virtual void DoRun (void);
  void SentPacket(std::string context, Ptr<const Packet> p );
  void ReceivedPacket(std::string context, Ptr<const Packet> p, const Address& addr);
  void Ratio();
};

/**
   * \brief Constructor
   * \param context - set the number of nodes
   * \param uint_32t - integer value defining number of nodes in the test
   */ 
TsiToolkitTestCase2::TsiToolkitTestCase2(uint32_t nNodes, uint32_t simulationTime, uint32_t mobilitySpeed)
: TestCase ("Test Case 2"),
  m_numberOfNodes (nNodes),
  m_simulationTime (simulationTime),
  m_mobility_speed (mobilitySpeed)
{
  m_packets_sent = 0;
  m_packets_received = 0; 
  m_bytes_sent = 0;
  m_bytes_received = 0;  
}

/**
   * \brief Each time the packet is sent, this function is called using TracedCallback and information about packet is stored in m_delayTable
   * \param context - used to display full path of the information provided (useful for taking information about the nodes or applications)
   * \param packet - Ptr reference to the packet itself
   */
void
TsiToolkitTestCase2::SentPacket(std::string context, Ptr<const Packet> p){
    /* 
    //HELP LINES USED FOR TESTING
    std::cout << "\n ..................SentPacket....." << p->GetUid() << "..." <<  p->GetSize() << ".......  \n";
    p->Print(std::cout);                
    std::cout << "\n ............................................  \n";  
    */

    //Sum bytes of the packet that was sent
    m_bytes_sent += p->GetSize(); 
    m_packets_sent++;

    //Insert in the delay table details about the packet that was sent
    m_delayTable.insert (std::make_pair (p->GetUid(), (double)Simulator::Now().GetSeconds()));
}

void
TsiToolkitTestCase2::ReceivedPacket(std::string context, Ptr<const Packet> p, const Address& addr){
    /*
    //HELP LINES USED FOR TESTING
    std::cout << "\n ..................ReceivedPacket....." << p->GetUid() << "..." <<  p->GetSize() << ".......  \n";
    p->Print(std::cout);                
    std::cout << "\n ............................................  \n";  
    */

    //Find the record in m_delayTable based on packetID
    std::map<uint32_t, double >::iterator i = m_delayTable.find ( p->GetUid() );
    
    //Get the current time in the temp variable
    double temp = (double)Simulator::Now().GetSeconds();  
    //Display the delay for the packet in the form of "packetID delay" where delay is calculated as the current time - time when the packet was sent
    std::cout << p->GetUid() << "\t" << temp - i->second << "\n";

    //Remove the entry from the delayTable to clear the RAM memroy and obey memory leakage
    if(i != m_delayTable.end()){
        m_delayTable.erase(i);
    }

    //Sum bytes and number of packets that were sent
    m_bytes_received += p->GetSize(); 
    m_packets_received++;
}

void
TsiToolkitTestCase2::Ratio(){

    std::cout << "Sent (bytes):\t" <<  m_bytes_sent
    << "\tReceived (bytes):\t" << m_bytes_received 
    << "\nSent (Packets):\t" <<  m_packets_sent
    << "\tReceived (Packets):\t" << m_packets_received 
    
    << "\nRatio (bytes):\t" << (float)m_bytes_received/(float)m_bytes_sent
    << "\tRatio (packets):\t" << (float)m_packets_received/(float)m_packets_sent << "\n";
}

// Add some help text to this case to describe what it is intended to test
TsiToolkitTestCase2::TsiToolkitTestCase2 ()
  : TestCase ("TsiToolkit test case 2 with random mobility"),
  m_numberOfNodes (3),
  m_simulationTime (300)
{
}

// This destructor does nothing but we include it as a reminder that
// the test case should clean up after itself
TsiToolkitTestCase2::~TsiToolkitTestCase2 ()
{
}

//
// This method is the pure virtual method from class TestCase that every
// TestCase must implement
//
void
TsiToolkitTestCase2::DoRun (void)
{
  //
  // Explicitly create the nodes required by the topology (shown above).
  // 
  NodeContainer nodes;
  nodes.Create (m_numberOfNodes); 

  // Set up WiFi
  WifiHelper wifi;

  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");        
  wifiChannel.AddPropagationLoss ("ns3::TwoRayGroundPropagationLossModel",
                              "SystemLoss", DoubleValue(1),
                              "HeightAboveZ", DoubleValue(1.5));

  // For range near 250m
  wifiPhy.Set ("TxPowerStart", DoubleValue(33));
  wifiPhy.Set ("TxPowerEnd", DoubleValue(33));
  wifiPhy.Set ("TxPowerLevels", UintegerValue(1));
  wifiPhy.Set ("TxGain", DoubleValue(0));
  wifiPhy.Set ("RxGain", DoubleValue(0));
  wifiPhy.Set ("EnergyDetectionThreshold", DoubleValue(-61.8));
  wifiPhy.Set ("CcaMode1Threshold", DoubleValue(-64.8));

  // Set 802.11a standard 
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate12Mbps"), "RtsCtsThreshold", UintegerValue (1492));
  wifiPhy.SetChannel (wifiChannel.Create ());

  // Add a non-QoS upper mac
  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
  wifiMac.SetType ("ns3::AdhocWifiMac");

  NetDeviceContainer devices;
  devices = wifi.Install (wifiPhy, wifiMac, nodes);

  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
                                 "X", StringValue ("100.0"),
                                 "Y", StringValue ("100.0"),
                                 "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=90]"));


  //merge int + string to create record "10s"
  std::stringstream tempSpeedVariable;
  tempSpeedVariable << m_mobility_speed << "s"; 

  //mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                             "Mode", StringValue ("Time"),
                             "Time", StringValue ( tempSpeedVariable.str() ),
                             "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=10.0]"),
                             "Bounds", StringValue ("0|1000|0|1000"));
  mobility.Install (nodes);
 
  InternetStackHelper internet;
  OlsrHelper routingProtocol;
  internet.SetRoutingHelper (routingProtocol);
  internet.Install (nodes);

  // Set up Addresses
  Ipv4AddressHelper ipv4; 
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer ifcont = ipv4.Assign (devices);
   
  uint16_t port = 9;   // Discard port (RFC 863)
  uint16_t serverPosition = m_numberOfNodes-1; 
  uint16_t clientPosition = 0;
 
  TsiClientHelper tsiClient ("ns3::UdpSocketFactory", InetSocketAddress (ifcont.GetAddress (serverPosition), port));
  tsiClient.SetAttribute("PacketSize", UintegerValue (150));

  ApplicationContainer apps = tsiClient.Install (nodes.Get (clientPosition));
  apps.Start (Seconds (15.0));
  apps.Stop (Seconds (m_simulationTime));

  // Create a packet tsiSink to receive these packets
  TsiSinkHelper tsiSink ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), port));
  apps = tsiSink.Install (nodes.Get (serverPosition));

  apps.Start (Seconds (15.0));
  apps.Stop (Seconds (m_simulationTime)); 
 
  Config::Connect("/NodeList/*/ApplicationList/*/$ns3::TsiClient/Tx", MakeCallback (&TsiToolkitTestCase2::SentPacket, this)); 
  Config::Connect("/NodeList/*/ApplicationList/*/$ns3::TsiSink/Rx",   MakeCallback(&TsiToolkitTestCase2::ReceivedPacket, this)); 


  Simulator::Stop (Seconds (500));
  Simulator::Run ();

  /*****************************
  * TEST COMMANDS
  *****************************/

  // A wide variety of test macros are available in src/core/test.h and on https://www.nsnam.org/doxygen/group__testing.html
  NS_TEST_ASSERT_MSG_GT (m_packets_sent, 30, "At least 30 packets should be sent");

  NS_TEST_ASSERT_MSG_GT (m_packets_received, 130, "At least 130 packets should be received");

  // Use this one for floating point comparisons
  NS_TEST_ASSERT_MSG_GT_OR_EQ ((float)m_packets_received/(float)m_packets_sent, 0.70, "At least 70\% of packet delivery ratio");


  Simulator::Destroy ();
}














// The TestSuite class names the TestSuite, identifies what type of TestSuite,
// and enables the TestCases to be run.  Typically, only the constructor for
// this class must be defined
//
class TsiToolkitTestSuite : public TestSuite
{
public:
  TsiToolkitTestSuite ();
};

TsiToolkitTestSuite::TsiToolkitTestSuite ()
  : TestSuite ("tsi-toolkit", UNIT)
{ 
  // TestDuration for TestCase can be QUICK, EXTENSIVE or TAKES_FOREVER
  AddTestCase (new TsiToolkitTestCase1(), TestCase::QUICK);
  AddTestCase (new TsiToolkitTestCase1(3, 300), TestCase::QUICK);
  AddTestCase (new TsiToolkitTestCase1(4, 300), TestCase::QUICK);

  AddTestCase (new TsiToolkitTestCase2(3, 300, 5), TestCase::QUICK);
  AddTestCase (new TsiToolkitTestCase2(4, 300, 10), TestCase::QUICK);
}

// Do not forget to allocate an instance of this TestSuite
static TsiToolkitTestSuite tsiToolkitTestSuite;