/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef TSI_TOOLKIT_H
#define TSI_TOOLKIT_H

#include <queue>
#include "ns3/packet.h"
#include "ns3/tag.h"
#include "ns3/object.h"
#include "ns3/node.h"
#include "ns3/ipv4-header.h"
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/header.h"
#include "ns3/simulator.h"
#include "ns3/tsi-header.h"


namespace ns3 {

 /**
     * \ingroup internet-queues
     *
     */
    class TsiToolkit : public Object {
    public:
  
        /**
        * \brief Get the type ID.
        * \return the object TypeId
        */
        static TypeId GetTypeId (void);
       
        TsiToolkit ();

        virtual ~TsiToolkit ();

        void setPacketRatio (TsiHeader header);
  
    protected: 
      /**
       * The dispose method. Subclasses must override this method
       * and must chain up to it by calling Node::DoDispose at the
       * end of their own DoDispose method.
       */
      virtual void DoDispose (void);

      virtual void DoInitialize (void);

    private:  
   
    }; 

}

#endif /* TSI_TOOLKIT_H */

