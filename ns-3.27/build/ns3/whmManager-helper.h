#include "ns3/object-factory.h"
#include "ns3/ipv4-address.h"
#include "ns3/node-container.h"
#include "ns3/application-container.h"
#include "ns3/whmManager.h"
#include "whmSend-helper.h"
#include "whmReceive-helper.h"

namespace ns3 {

class whmSendHelper;
class whmReceiveHelper;

class whmManagerHelper
{
public:
 
  whmManagerHelper (std::string protocol, Address address);
 
 // whmManagerHelper (uint16_t port);

  void SetAttribute (std::string name, const AttributeValue &value);

 // void SetConstantRate (DataRate dataRate, uint32_t packewhmze = 512);

  ApplicationContainer Install (NodeContainer c) const;

  ApplicationContainer Install (Ptr<Node> node) const;

  ApplicationContainer Install (std::string nodeName) const;
  
  uint32_t m_numberOfFlows;

private:
  
  Ptr<Application> InstallPriv (Ptr<Node> node) const;
  ObjectFactory m_factory; //!< Object factory.
  void IncreaseFlowNumber();
};

} // namespace ns3


