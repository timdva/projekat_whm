#ifndef whm_CLIENT_H
#define whm_CLIENT_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/ipv4-address.h"
#include "ns3/traced-callback.h"

namespace ns3 {

class Socket;
class Packet;

class whmSend : public Application
{
public:
 
  static TypeId GetTypeId (void);

  whmSend ();

  virtual ~whmSend ();

  void SetRemote (Address ip, uint16_t port);
 
  void SetRemote (Address addr); 

protected:
  virtual void DoDispose (void);

private:

  virtual void StartApplication (void);
  virtual void StopApplication (void);

  void Send (void);
  void HandleRead (Ptr<Socket> socket);


  uint32_t m_flow;
  uint32_t m_count; //!< Maximum number of packets the application will send
  Time m_interval; //!< Packet inter-send time
  uint32_t m_size; //!< Size of the sent packet (including the SeqTsHeader)

  uint16_t m_sent; //!< Counter for sent packets
  uint16_t m_received_ack; //!< dodano
  Ptr<Socket> m_socket; //!< Socket
  Address m_peerAddress; //!< Remote peer address
  uint16_t m_peerPort; //!< Remote peer port
  EventId m_sendEvent; //!< Event to send the next packet
  TracedCallback<Ptr<const Packet>> m_txTrace;
  TracedCallback<Ptr<const Packet>, const Address &> m_tx_ackTrace; //dodano


};

} // namespace ns3

#endif /* whm_CLIENT_H */
