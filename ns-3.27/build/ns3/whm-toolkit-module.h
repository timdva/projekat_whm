
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_WHM_TOOLKIT
    

// Module headers:
#include "whm-header.h"
#include "whm-toolkit-helper.h"
#include "whm-toolkit.h"
#endif
