#ifndef whm_MANAGER_H
#define whm_MANAGER_H

#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/traced-callback.h"
#include "ns3/address.h"
#include "whmReceive.h"
#include "whmSend.h"
#include "ns3/whmReceive-helper.h"
#include "ns3/whmSend-helper.h"


namespace ns3 {

class Address;
class Socket;
class Packet;
class whmSend;
class whmReceive;


class whmManager
{
public:

whmManager ();

uint16_t ETX_metric (whmReceive R, whmSend S);


private:

  uint16_t        m_sent_ack;      //!< dodano
  uint16_t        m_received;      //!< dodano
  uint16_t        m_sent;          //!< Counter for sent packets
  uint16_t        m_received_ack;  //!< dodano
 
};

} // namespace ns3

#endif /* whm_MANAGER_H */
