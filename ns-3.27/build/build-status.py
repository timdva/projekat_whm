#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/ns3.27-whm_test_1-debug', 'build/scratch/subdir/ns3.27-subdir-debug', 'build/scratch/ns3.27-whm_test_3-debug', 'build/scratch/ns3.27-scratch-simulator-debug', 'build/scratch/ns3.27-whm_test_zadnji-debug', 'build/scratch/ns3.27-whm_test_2-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

