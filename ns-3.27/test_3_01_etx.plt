set terminal png
set output "test_3_01_etx.png"

set title "ETX for 3 nodes and link 1-0 with loss"

set ylabel "ETX value"
set xlabel "Time (second)"

set border linewidth 2 
set style line 1 linecolor rgb 'red' linetype 1 linewidth 1
set style line 2 linecolor rgb 'blue' linetype 1 linewidth 1  
set grid ytics  
set grid xtics 
plot "drugi_test_2_etx_01.dat" using 1:2 title "ETX" with linespoints  ls 2

