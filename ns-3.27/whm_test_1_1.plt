set terminal png
set output "whm_test_1_1.png"

set title "ETX for 2 nodes"

set ylabel "ETX value"
set xlabel "Time (second)"

set border linewidth 2 
set style line 1 linecolor rgb 'red' linetype 1 linewidth 1 
set grid ytics  
set grid xtics 
plot "prvi_test_etx.dat" using 1:2 title "ETX" with linespoints  ls 2

