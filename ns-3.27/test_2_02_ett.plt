set terminal png
set output "test_2_02_ett.png"

set title "ETT for 3 nodes and link 2-0"

set ylabel "ETT value"
set xlabel "Time (second)"

set border linewidth 2 
set style line 1 linecolor rgb 'red' linetype 1 linewidth 1
set style line 2 linecolor rgb 'blue' linetype 1 linewidth 1  
set grid ytics  
set grid xtics 
plot "drugi_test_1_ett_02.dat" using 1:2 title "ETT" with linespoints  ls 1

